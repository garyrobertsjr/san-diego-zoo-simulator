#! /usr/bin/env python
# Gary Roberts
# g_roberts@u.pacific.edu
# 
# COMP173 Final Project: San Diego Zoo

import argparse

from Zoo import Zoo

# ArgParse Config
init_parser = argparse.ArgumentParser(description='Emulate the San Diego Zoo')
init_parser.add_argument('file', type=argparse.FileType('r'))
args = init_parser.parse_args()

# Parses each line from file as a list with commas stripped.
arguments = [line.replace(',', ' ').split() for line in args.file]

# Ignore all execution strings that start with a 0 value for M
for argument in arguments:
	if argument[0] == '0':
		arguments.remove(argument)

# Debug
print('Accepted Arguments: ' + str(arguments))

# Main Runtime
for argument in arguments:
	zoo = Zoo(argument)
	try:
		zoo.run()
	except KeyboardInterrupt as exception:
		print('Caught Control Break')
