import threading
import time
import sys

serving = True    # Global Flag for thread runtime control
cars = [] 	  # List of inactive car objects.
people = []       # List for holding people objects prior to car handoff
ride_queue = []   # List people objs waiting for avail car
refuel_queue = [] # List of cars waiting for refuel
elapsed = 0       # Global counter for time elapsed.

# Condition Vars to be implemented as locks. Paired with queues of defined max size to implement
# a counting semaphore.
car_cv = threading.Condition()
gas_cv = threading.Condition()


# To be implemented as name container to be passed among threads.
class Person:
	def __init__(self, name):
		self.name = name

# To be implemented as a car contianers to track refueling.
class Pump:
	def __init__(self, name):
		self.name = name
		self.refuel_status = 0
		self.serving = None


# To be implemented as car object to track gas levels and occupant.
class Vehicle:
	def __init__(self, name):
		self.name = name
		self.gas_level = 5
		self.occupant = None
		self.duration = 0


class Zoo:
	def __init__(self, args):
		global avail_car, avail_gas
		# Counter vars to act as counting semaphores paired with a CV
		self.M = int(args[0]) # Num visitors
		self.N = int(args[1]) # Num cars
		self.T = int(args[2]) # Duration of ride
		self.K = int(args[3]) # Num gas pumps
		
	def run(self):
		global cars, elapsed, serving
		serving = True
		cars = []
		elapsed = 0
		# Init cars
		for i in range(self.N):
			new_car = Vehicle(i)
			cars.append(new_car)
			
		visitor = Visitor(self.M)
		car = Car(self.N, self.T, self.M)
		gas = Gas(self.K)
		
		car.start()
		gas.start()
		visitor.start()
		car.join()
		gas.join()

class Visitor(threading.Thread):
	def __init__(self, M):
		self.M = M
		threading.Thread.__init__(self)
	def run(self):
		global elapsed, people, ride_queue, serving
		# Keep running until all customers have been handed off to Car thread.
		
		# Create person objects and populate queue.
		for i in range(self.M):
			new_person = Person(i)
			people.append(new_person)	
		
		while serving:
			car_cv.acquire()
				
			print('----------------------------------------')
			print('Status Report')
			print('Elapsed: ' + str(elapsed))
			# Need to change this to feed in people with random arrivals??
			for i in range(8):
				if people != []:
					print('Customer ' + str(people[0].name) + ' has arrived.')
					ride_queue.append(people.pop(0))

			car_cv.notifyAll()
			car_cv.wait()
	
			# Tick the clock
			elapsed += 1
			print('----------------------------------------\n')
			car_cv.release()

		print('Visitor Terminated')


class Car(threading.Thread):
	def __init__(self, N, T, M):
		self.N = N
		self.T = T
		self.M = M
		self.served = 0
		self.dispatched = []
		threading.Thread.__init__(self)
	def run(self):
		global refuel_queue, ride_queue, serving
		while serving:
			car_cv.acquire()
			car_cv.wait()
			
			# Fill cars
			while len(self.dispatched) != self.N and \
			      len(ride_queue) != 0 and cars != []:
				# Get available car and put person obj in it
				cars[0].occupant = ride_queue.pop(0)
				# Add newly populated car to dispatched queue
				self.dispatched.append(cars.pop(0))
			print('Waiting for ride: ' + str([x.name for x in ride_queue]))	
			print('Dispatched cars : ' + str([x.name for x in self.dispatched]))
			print('Occupied with   : ' + str([x.occupant.name for x in self.dispatched]))
			finished = []

			for car in self.dispatched:
				# Unload cars from completed rides
				if car.duration == self.T:
					finished.append(car)
				# Deduce ride countdown by 1
				car.duration += 1

			for car in finished:
				temp = car
				self.dispatched.remove(car)
				temp_occ = temp.occupant
				
				# Person obj leaves car
				temp.occupant = None
				temp.duration = 0

				# Consume gas for ride
				temp.gas_level -= 1

				# Check gas level
				if temp.gas_level == 0:
					# Send car to refuel
					refuel_queue.append(temp)
				else:
					# Car added back to free queue
					cars.append(temp)	

				print(str(temp_occ.name) + ' gets off ride and leaves park')
					
				self.served += 1
			# Tick Gas
			gas_cv.acquire()
			gas_cv.notifyAll()
			gas_cv.wait()
			gas_cv.release()
			
			if self.served == self.M:
				serving = False

			car_cv.notifyAll()
			car_cv.release()

		# Notify gas thread to die
		gas_cv.acquire()
		gas_cv.notifyAll()
		gas_cv.release()		
		print('Car Terminated')


class Gas(threading.Thread):
	def __init__(self, K):
		self.gas_level = 200
		self.K = K
		self.free_pumps = []
		self.occ_pumps = []
		self.refueling = False
		self.tanker_status = 0
		threading.Thread.__init__(self)
		
	def run(self):
		global refuel_queue, serving
		
		# Init pumps
		for i in range(self.K):
			new_pump = Pump(i)
			self.free_pumps.append(new_pump)
		
		while serving:
			gas_cv.acquire()
			
			# Sleep until car arrives from car thread.
			gas_cv.wait()
			
			# Assign cars to pumps until full or no cars waiting
			# If tanker is waiting, don't occupy any more pumps.
			while len(refuel_queue) != 0 and len(self.occ_pumps) < self.K and self.refueling is False:
				temp = self.free_pumps.pop(0)
				temp.serving = refuel_queue.pop(0)
				self.occ_pumps.append(temp)

			
			# Tick clock and return filled cars
			for pump in self.occ_pumps:
				pump.refuel_status += 1
				if pump.refuel_status == 3:
					self.gas_level -= 5
					# Call for refuel	
					if self.gas_level <= 150:
						self.refueling = True

					if self.gas_level <= 0:
						print('STARVATION DETECTED: Cars at pumps but pump is depleted.')
						sys.exit()

					pump.refuel_status = 0
					pump.serving.gas_level = 5 
					temp = self.occ_pumps.pop(self.occ_pumps.index(pump))
		
					# Return car to queue
					print(str(temp.serving.name) + ' has been refueled.')
					cars.append(temp.serving)
					temp.serving = None

					# Return pump to queue
					print('Pump ' + str(temp.name) + ' is now free.')
					self.free_pumps.append(temp)
			
			# Fuel for one time unit if no cars are occupying pumps.	
			if self.refueling is True and self.occ_pumps == []:
				print('Tanker has refueled ' + str(self.tanker_status) + ' of 15 time units.')
				self.tanker_status += 1
				if self.tanker_status >= 15:
					self.tanker_status = 0
					self.refueling = False
					self.gas_level = 200

			print('Gas Level       : ' + str(self.gas_level))	
			print('Waiting for Pump: ' + str([x.name for x in refuel_queue]))
			print('Fueling         : ' + str([x.serving.name for x in self.occ_pumps]))			
					
			gas_cv.notifyAll()
			gas_cv.release()
		print('Gas terminated.')			
